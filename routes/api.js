const appController = require('../app/controllers/AppController')
var express = require('express');
var router = express.Router();


router.get('/planets', appController.index);

module.exports = router;