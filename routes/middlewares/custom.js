"use strict";
const error_types = require("./error_types");
var fs = require("fs");

function stringify(obj) {
  let cache = [];
  let str = JSON.stringify(obj, function (key, value) {
    if (typeof value === "object" && value !== null) {
      if (cache.indexOf(value) !== -1) {
        // Circular reference found, discard key
        return;
      }
      // Store value in our collection
      cache.push(value);
    }
    return value;
  });
  cache = null; // reset the cache
  return str;
}

function createObject(req) {
    return {
        headers: req.headers,
        url: req.url,
        method: req.method,
        params: req.params,
        query: req.query,
        body: req.body,
    };
}

let middlewares = {
  errorHandler: (error, req, res, next) => {
    // console.log("ejecutando middleware de control de errores");
    if (error instanceof error_types.InfoError)
      res.status(200).json({ error: error.message });
    else if (error instanceof error_types.Error404)
      res.status(404).json({ error: error.message });
    else if (error instanceof error_types.Error403)
      res.status(403).json({ error: error.message });
    else if (error instanceof error_types.Error401)
      res.status(401).json({ error: error.message });
    else if (error.name == "ValidationError")
      //de mongoose
      res.status(200).json({ error: error.message });
    else if (error.message) res.status(500).json({ error: error.message });
    else next();
  },
  notFoundHandler: (req, res, next) => {
    console.log(
      "ejecutando middleware para manejo de endpoints no encontrados"
    );
    const data = {
      messaje: "Error Message : endpoint not found",
      req: createObject(req),
    };

    res.status(404).json({ error: "endpoint not found" });
  },
};

module.exports = middlewares;
