class AppController {
    async index(req, res, next) {
        try {
            let url = 'https://swapi.dev/api/planets/';
            if(req.query.page){
                url = url + "?page="+ req.query.page
            }
            axios.get(url).then(response =>{ 
                res.status(200).json(response.data)
            }).catch(error=>{
                next()
            })
        } catch (error) {
            next()
        }
    }
}

const appController = new AppController();
module.exports = appController;