const express = require('express')
const app = express()
var cors = require("cors");
const port = 3000
global.axios = require('axios');

const Middlewares = require('./routes/middlewares/custom')
const ApiRouter = require('./routes/api');

app.use(cors());

app.use("/api", ApiRouter);

app.use(Middlewares.errorHandler);
app.use(Middlewares.notFoundHandler);

console.log("HELLO");

app.listen(port, () => {
  console.log(`App listening on port ${port}`)
})